import { Flex, Grid, Text, Button, BoxProps, Box } from "@theme-ui/components";
import Image from "next/image";
import React, { FC, memo, SVGProps, useMemo } from "react";
import { ParsedUrlQuery } from "querystring";
import { FacebookShareButton } from "react-share";
import { GetStaticPaths, InferGetStaticPropsType, NextPage } from "next";
import { CardProps } from "../../components/ProductCard";
import Layout from "../../containers/Layout";
import { fromImageToUrl } from "../../utils/urls";
import { formatCurrency } from "../../utils/format";
import useCart from "../../hooks/useCart";
import { getStaticPropsFunc } from "../../queries/getStaticProps";
import { CartIcon } from "../../public/icon";
import {
  params,
  prefetchData,
  prefetchQuery,
  useFetchQuery,
} from "../../queries";

const ProtectionIcon: FC<SVGProps<SVGSVGElement>> = (props) => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M3 4.12782L12.0043 1.5L21 4.12782V9.51684C21 15.1811 17.3751 20.2097 12.0013 22.0003C6.62604 20.2097 3 15.18 3 9.51435V4.12782Z"
      stroke="black"
      strokeOpacity="0.65"
      strokeWidth="2"
      strokeLinejoin="round"
    />
  </svg>
);

interface UtilitiesProps extends BoxProps {
  label?: string;
}
const Utilities: FC<UtilitiesProps> = ({ label, ...boxProps }) => (
  <Flex
    bg="productType7"
    sx={{
      pt: 35,
      width: 150,
      height: 140,
      borderRadius: 20,
      alignItems: "center",
      flexShrink: 0,
      flexDirection: "column",
      boxShadow: "rgb(137 140 149 / 50%) 1px 4px 5px",
    }}
    {...boxProps}
  >
    <CartIcon width={40} height={40} />
    <Text
      mt={22}
      sx={{
        textTransform: "uppercase",
        fontSize: 14,
        fontWeight: 600,
        textAlign: "center",
      }}
    >
      {label}
    </Text>
  </Flex>
);

export interface Params extends ParsedUrlQuery {
  id: string;
}
const detail: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({
  id,
}) => {
  const { data: product } = useFetchQuery<CardProps[], params>("/products", {
    id: id,
  });
  const currentUrl = useMemo(
    () => `https://bakery-app.vercel.app/products/${id}`,
    [id]
  );
  const [_, addToCart] = useCart();
  return (
    <Layout>
      <Flex
        sx={{
          bg: "white",
          width: "100%",
          justifyContent: "center",
        }}
      >
        <Grid
          columns={[1, 1, 2, 2, 2]}
          gap={[20, 20, 10, 40, 40]}
          sx={{
            py: 60,
            width: 1170,
            flexDirection: "column",
          }}
        >
          <Flex
            sx={{
              m: 15,
              borderRadius: 20,
              border: "1px solid #d3d1d1",
              alignItems: "center",
              justifyContent: "center",
              overflow: "hidden",
              ":hover": {
                ".image": {
                  transform: "scale(1.1)",
                },
              },
            }}
          >
            <Box
              p={15}
              className="image"
              sx={{
                transition: "0.3s",
                position: "relative",
                width: 565,
                height: 500,
              }}
            >
              <Image
                src={fromImageToUrl(product[0]?.image)}
                alt="Product"
                layout="fill"
                objectFit="scale-down"
              />
            </Box>
          </Flex>
          <Flex
            sx={{
              flexDirection: "column",
              mx: 15,
              alignItems: [
                "center",
                "center",
                "flex-start",
                "flex-start",
                "flex-start",
              ],
            }}
          >
            <Text
              mb={20}
              sx={{
                letterSpacing: "2px",
                color: "filter",
                fontSize: 30,
                fontWeight: 700,
              }}
            >
              {product[0]?.name}
            </Text>
            <Flex
              mb={20}
              sx={{
                letterSpacing: "2px",
                color: "productType1",
                alignItems: "end",
                fontSize: 22,
                fontWeight: 700,
              }}
            >
              {product[0]?.priceOld > 0 && (
                <Text
                  sx={{
                    textDecoration: "line-through",
                    letterSpacing: "2px",
                    color: "productPriceOld",
                    fontSize: 18,
                    fontWeight: 700,
                  }}
                >
                  {formatCurrency(product[0]?.priceOld)} <Text as="sup">đ</Text>
                </Text>
              )}
              <Text ml={20}>
                {formatCurrency(product[0]?.price)}
                <Text as="sup">đ</Text>
              </Text>
            </Flex>
            <Text
              mb={30}
              sx={{
                color: "productType2",
                opacity: 0.8,
                fontSize: 14,
                letterSpacing: "0.5px",
              }}
            >
              {product[0]?.descriptions}
            </Text>
            <Text
              mb={20}
              sx={{
                color: "productType2",
                fontWeight: 600,
                fontSize: 16,
                letterSpacing: "0.5px",
              }}
            >
              Utilities:
            </Text>
            <Flex mb={30}>
              <Utilities mr={20} label="Fast shipping" />
              <Utilities label="No deposit required" />
            </Flex>
            <Flex
              sx={{
                flex: 1,
                flexDirection: "column",
                justifyContent: "end",
              }}
            >
              <Flex mx={[15, 15, 0, 0, 0]} mb={30}>
                <Flex mr={[15, 15, 30, 30, 30]}>
                  <Button
                    onClick={() => addToCart(product[0]?.id)}
                    variant="secondary"
                    sx={{
                      width: [165, 160, 180, 180, 180],
                      height: 65,
                      boxShadow: "rgb(137 140 149 / 50%) 1px 4px 15px",
                    }}
                  >
                    ADD TO CART
                  </Button>
                </Flex>

                <Flex>
                  <FacebookShareButton url={currentUrl}>
                    <Button
                      as="div"
                      variant="primary"
                      sx={{
                        pb: 3,
                        width: [165, 160, 180, 180, 180],
                        height: 65,
                        color: "productPriceOld",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        boxShadow: "rgb(137 140 149 / 20%) 1px 4px 15px",
                      }}
                    >
                      SHARE
                    </Button>
                  </FacebookShareButton>
                </Flex>
              </Flex>
              <Flex
                mx={[15, 15, 0, 0, 0]}
                sx={{
                  pt: 15,
                  borderTop: "1px solid",
                  borderTopColor: "productType8",
                  color: "productType2",
                }}
              >
                <ProtectionIcon />
                <Text
                  ml={17}
                  sx={{
                    fontSize: 15,
                    letterSpacing: "0.5px",
                  }}
                >
                  Ensure quality and safety for customers.
                </Text>
              </Flex>
            </Flex>
          </Flex>
        </Grid>
      </Flex>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const products = await prefetchData<CardProps[]>("/products");
  const paths = products.map((item) => ({
    params: { id: item.id.toString() },
  }));

  return {
    paths,
    fallback: "blocking",
  };
};

export const getStaticProps = getStaticPropsFunc(
  async ({ params, queryClient }) => {
    const { id } = params as Params;
    await prefetchQuery(queryClient, "/products", {
      id: id,
    });
    return {
      id,
    };
  }
);

export default memo(detail);
