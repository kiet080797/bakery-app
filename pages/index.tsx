import { Box, Button, Flex, Grid, Text } from "@theme-ui/components";
import { NextPage } from "next";
import Link from "next/link";
import Image from "next/image";
import React, { memo, useCallback, useState } from "react";
import ChooseUs, { ChooseUsProps } from "../components/ChooseUs";
import { ElementItem } from "../components/ElementsCard";
import { HeaderProps } from "../components/Header";
import ListEmpty from "../components/ListEmpty";
import { ItemProps } from "../components/Menu";
import OfferCard, { OfferCardProps } from "../components/OfferCard";
import ProductCard, { CardProps } from "../components/ProductCard";
import Layout from "../containers/Layout";
import { prefetchQuery, useFetchQuery } from "../queries/fetchData";
import { getStaticPropsFunc } from "../queries/getStaticProps";
import { Arrow, CardIcon, Cookie, Flower, Van } from "../public/icon";

import image from "../public/images/bg-hero.jpg";
import promo from "../public/images/promo.jpg";
import chooseUs from "../public/images/why-choose-us.jpg";
import about from "../public/images/about.jpg";
import parallax from "../public/images/parallax-bg.jpg";
import partner1 from "../public/images/partner-1.png";
import partner2 from "../public/images/partner-2.png";
import partner3 from "../public/images/partner-3.png";
import partner4 from "../public/images/partner-4.png";
import partner5 from "../public/images/partner-5.png";
import offer1 from "../public/images/offer-1.jpg";
import offer2 from "../public/images/offer-2.jpg";
import offer3 from "../public/images/offer-3.jpg";
import partner from "../public/images/bg-partner.jpg";

//menu
export const menuItems: ItemProps[] = [
  { id: "1", label: "HOME" },
  {
    id: "2",
    label: "GALLERY",
    content: [
      "GRID GALLERY",
      "GRID GALLERY",
      "MASONRY GALLERY",
      "MASONRY FULLWIDTH",
    ],
  },
  {
    id: "3",
    label: "SHOP",
    content: [
      "GRID SHOP",
      "SHOP LIST",
      "SINGLE PRODUCT",
      "CART PAGE",
      "CHECK OUT",
    ],
  },
  { id: "4", label: "BLOG", content: ["GRID BLOG", "BLOG LIST", "BLOG POST"] },
  {
    id: "5",
    label: "PAGES",
    content: ["WHAT WE OFFER", "OUR TEAM", "TESTIMONIALS", "PRICING LIST"],
  },
  {
    id: "6",
    label: "ELEMENTS",
    content: [],
  },
  { id: "7", label: "CONTACT US" },
];

//elements in menu
export const elements: ElementItem = {
  element: [
    "TYPOGRAPHY",
    "ICON LISTS",
    "PROGRESS BARS",
    "CALLS TO ACTION",
    "TABS & ACCORDIONS",
    "BUTTONS",
    "TABLES",
    "FORMS",
    "COUNTERS",
    "GRID SYSTEM",
  ],
  additional: ["404 PAGE", "COMING SOON", "PRIVACY POLICY", "SEARCH RESULTS"],
};

//contact
export const contact: HeaderProps = {
  street: "111 Đặng Thùy Trâm",
  address: "Phường 13, Quận Bình Thạnh, TP.HCM",
  telephone: "+(84) 347.111.111",
  mail: "test@gmail.com",
};

//offer session
const offerItems: OfferCardProps[] = [
  {
    id: 1,
    image: offer1,
    label: "PARTY CUPCAKES",
    description:
      "We provide a variety of cupcakes for any party made with high-quality natural ingredients and no preservatives.",
  },
  {
    id: 2,
    image: offer2,
    label: "WEDDING CAKES",
    description:
      "Nothing tastes better than a chocolate cake with a variety of flavors, which is always available at our bakery.",
  },
  {
    id: 3,
    image: offer3,
    label: "CHOCO CAKES",
    description:
      "Want to make your wedding unforgettable? Then you need to order a unique wedding cake at Sweet Bakery!",
  },
];
// choose-us session
const ChooseUsItems: ChooseUsProps[] = [
  {
    id: "1",
    label: "QUALITY PRODUCTS",
    content:
      "We guarantee the quality of all the cakes we provide as they are baked using the freshest ingredients.",
  },
  {
    id: "2",
    label: "FREE DELIVERY",
    content:
      "We guarantee the quality of all the cakes we provide as they are baked using the freshest ingredients.",
  },
  {
    id: "3",
    label: "CATERING SERVICE",
    content:
      "Our bakery also provides an outstanding catering service for events and special occasions.",
  },
  {
    id: "4",
    label: "ONLINE PAYMENT",
    content:
      "We accept all kinds of online payments including Visa, MasterCard, American Express credit cards.",
  },
];

//type object about-us session
interface AboutUsProps {
  id?: number;
  button?: string;
  label?: string;
  href?: string;
  content?: string;
}

// about-us session
const AboutUsItems: AboutUsProps[] = [
  {
    id: 1,
    button: "OUT MISSION",
    label: "PROVIDING QUALITY BAKED GOODS FOR ALL CUSTOMERS",
    href: "#",
    content:
      "Our mission is to create a bakery that makes the best quality baked goods on site from scratch, and where both employees and customers would feel comfortable.",
  },
  {
    id: 2,
    button: "OUR VALUES",
    label: "ENSURING THE BEST ATMOSPHERE FOR EVERYONE",
    href: "#",
    content:
      "We see the most important part of our business in ensuring the happiness of our staff and the satisfaction of our clients by creating a welcoming and caring atmosphere.",
  },
  {
    id: 3,
    button: "OUR GOALS",
    label: "SERVING ONLY THE FRESHEST BAKED GOODS FOR YOU",
    href: "#",
    content:
      "We aim to deliver the best baked goods for corporate events and individual celebrations, while also offering the best level of customer service in the United States.",
  },
];

//index page
const index: NextPage = () => {
  //use useQuery in react-query to get products
  const { data: products, isLoading } = useFetchQuery<CardProps[]>("/products");
  //page in about-us session
  const [page, setPage] = useState<number>(1);
  //set page when you click
  const onPage = useCallback((id: number) => {
    setPage(id);
  }, []);

  return (
    //layout component (header, footer)
    <Layout>
      {/* delicious session */}
      <Flex
        sx={{
          height: [400, 400, 500, 500, 500],
          position: "relative",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            width: "100%",
            height: "100%",
            top: 0,
            left: 0,
          }}
        >
          <Box
            sx={{
              position: "relative",
              width: "100%",
              height: "100%",
              opacity: [0.3, 0.3, 0.3, 1, 1],
            }}
          >
            <Image
              src={image}
              alt="panner"
              layout="fill"
              placeholder="blur"
              objectFit="cover"
            />
          </Box>
        </Box>
        <Flex
          pt={[50, 50, 80, 80, 80]}
          sx={{
            width: "100%",
            zIndex: 1,
            justifyContent: "center",
          }}
        >
          <Flex
            mx={15}
            sx={{
              width: 1170,
              flexDirection: "column",
              overflow: "hidden",
              alignItems: [
                "center",
                "center",
                "flex-start",
                "flex-start",
                "flex-start",
              ],
              textAlign: ["center", "center", "left", "left", "left"],
            }}
          >
            <Text variant="ImageLabel">Delicious</Text>
            <Text variant="ImageContent">CAKES FOR YOU</Text>
            <Text variant="ImageDescription">
              Sweet Bakery offers the best cakes and sweets for you.
            </Text>
            <Link href={`/products`} passHref>
              <Flex>
                <Button
                  p={[
                    "17px 40px",
                    "17px 40px",
                    "25px 50px",
                    "25px 50px",
                    "25px 50px",
                  ]}
                  variant="secondary"
                  sx={{ boxShadow: "#bebebe 0px 5px 12px 0px" }}
                >
                  SHOP NOW
                </Button>
              </Flex>
            </Link>
          </Flex>
        </Flex>
      </Flex>
      {/* only-fresh-cakes session */}
      <Flex
        sx={{
          px: 15,
          pt: 15,
          position: "relative",
          width: "100%",
          height: [390, 390, 530, 530, 530],
          justifyContent: "center",
          top: -45,
          overflow: "hidden",
        }}
      >
        <Flex
          sx={{
            position: "relative",
            width: 1170,
          }}
        >
          <Flex
            p={10}
            bg="#f3f7f8"
            sx={{
              width: "100%",
              height: [350, 350, 490, 490, 490],
              zIndex: 1,
              position: "absolute",
              boxShadow: [
                "#969696 0px 5px 15px 0px",
                "#969696 0px 5px 15px 0px",
                "#969696 0px 10px 15px 0px",
                "#969696 0px 10px 15px 0px",
                "#969696 0px 10px 15px 0px",
              ],
              ":hover": {
                ".button": {
                  transform: "scale(1)",
                  opacity: 1,
                },
              },
            }}
          >
            <Box
              sx={{
                position: "relative",
                width: "100%",
                height: "100%",
              }}
            >
              <Image
                src={promo}
                alt="secondPanner"
                layout="fill"
                objectFit="cover"
                placeholder="blur"
              />
            </Box>

            <Flex
              sx={{
                flexDirection: "column",
                width: [300, 450, 540, 540, 540],
                position: "absolute",
                left: "50%",
                transform: "translateX(-50%)",
                alignItems: "center",
                textAlign: "center",
              }}
            >
              <Text mt={[40, 50, 60, 60, 60]} variant="ImageLabel1">
                Only Fresh Cakes
              </Text>
              <Text variant="ImageContent1">
                All of our products are made from scratch using family recipes
                with only the highest quality ingredients. We bake and sell
                fresh daily to ensure only the best products are sold to our
                customers.
              </Text>
              <Link href="/products" passHref>
                <Button
                  className="button"
                  variant="secondary"
                  aria-label="arrow"
                  pt={4}
                  sx={{
                    width: 50,
                    height: 50,
                    borderRadius: 99,
                    opacity: 0.5,
                    transform: "scale(0.5)",
                    boxShadow: "#959da5 0px 12px 24px 0px",
                  }}
                >
                  <Arrow width={15} height={15} />
                </Button>
              </Link>
            </Flex>
          </Flex>
        </Flex>
      </Flex>
      {/* offer session */}
      <Flex
        sx={{
          width: "100%",
          justifyContent: "center",
        }}
      >
        <Flex
          sx={{
            p: "30px 0 120px",
            width: 1170,
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Text mb={60} variant="offerLabel1">
            WHAT WE OFFER
          </Text>

          <Grid
            columns={[1, 1, 3, 3, 3]}
            gap={30}
            sx={{
              px: 15,
              /* overflow: "hidden", */
            }}
          >
            {offerItems.map((item) => (
              <Flex key={item.id}>
                <OfferCard
                  label={item.label}
                  image={item.image}
                  description={item.description}
                />
              </Flex>
            ))}
          </Grid>
        </Flex>
      </Flex>
      {/* products */}
      <Flex
        sx={{
          bg: "#ebebeb",
          width: "100%",
          justifyContent: "center",
        }}
      >
        <Flex
          sx={{
            py: 60,
            width: 1170,
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Text mb={60} mt={[20, 20, 60, 60, 60]} variant="offerLabel1">
            NEW PRODUCTS
          </Text>
          {products?.length ? (
            <Grid gap={0} columns={[1, 2, 2, 3, 4]}>
              {products
                ?.filter((x) => x.productType === "new")
                .map((item) => (
                  <Flex
                    key={item.id}
                    sx={{
                      p: 15,
                      height: 460,
                    }}
                  >
                    <ProductCard item={item} />
                  </Flex>
                ))}
            </Grid>
          ) : (
            <Flex m="40px 0 80px">
              <ListEmpty />
            </Flex>
          )}
          <Link href="/products">
            <Button
              my={30}
              p="25px 50px"
              variant="secondary"
              sx={{
                boxShadow: "#bbbbbb 0px 12px 24px 0px",
              }}
            >
              VIEW ALL
            </Button>
          </Link>
        </Flex>
      </Flex>
      {/* choose-us session */}
      <Flex
        sx={{
          width: "100%",
          justifyContent: "center",
        }}
      >
        <Flex
          sx={{
            py: 60,
            width: 1170,
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Text mb={60} mt={[20, 20, 60, 60, 60]} variant="offerLabel1">
            WHY CHOOSE US
          </Text>
          <Grid columns={[1, 1, "5fr 0 5fr", 3, 3]} gap={[20, 20, 20, 0, 0]}>
            <Flex sx={{ flexDirection: "column" }}>
              {ChooseUsItems.filter(
                (item) => item.id === "1" || item.id === "2"
              ).map((item) => (
                <ChooseUs
                  label={item.label}
                  content={item.content}
                  key={item.id}
                  id={item.id}
                >
                  {item.id === "1" ? (
                    <Cookie width={45} height={45} />
                  ) : (
                    <Van width={45} height={45} />
                  )}
                </ChooseUs>
              ))}
            </Flex>
            <Flex
              sx={{
                transition: "0.3s",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Box
                sx={{
                  position: "relative",
                  width: [0, 0, 0, 350, 350],
                  height: [0, 0, 0, 200, 200],
                }}
              >
                <Image src={chooseUs} alt="whyChooseUs" placeholder="blur" />
              </Box>
            </Flex>
            <Flex sx={{ flexDirection: "column" }}>
              {ChooseUsItems.filter(
                (item) => item.id === "3" || item.id === "4"
              ).map((item) => (
                <ChooseUs
                  label={item.label}
                  content={item.content}
                  key={item.id}
                  id={item.id}
                >
                  {item.id === "3" ? (
                    <Flower width={45} height={45} />
                  ) : (
                    <CardIcon width={45} height={45} />
                  )}
                </ChooseUs>
              ))}
            </Flex>
          </Grid>
        </Flex>
      </Flex>
      {/* summer session */}
      <Flex
        sx={{
          width: "100%",
          position: "relative",
          justifyContent: "center",
          bg: "transparent",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            width: "100%",
            height: "100%",
            top: 0,
            left: 0,
          }}
        >
          <Box
            sx={{
              position: "relative",
              opacity: [0.3, 0.3, 0.3, 1, 1],
              width: "100%",
              height: "100%",
            }}
          >
            <Image
              src={parallax}
              alt="parallax"
              layout="fill"
              placeholder="blur"
              objectFit="cover"
            />
          </Box>
        </Box>
        <Flex
          sx={{
            py: 60,
            width: 1170,
            justifyContent: ["center", "center", "center", "left", "left"],
          }}
        >
          <Flex
            sx={{
              mx: 15,
              transition: "0.3s",
              flexDirection: "column",
              width: ["80%", "80%", "45%", "45%", "45%"],
            }}
          >
            <Text
              mt={[0, 0, 60, 60, 60]}
              variant="offerLabel1"
              sx={{
                textAlign: ["center", "center", "center", "left", "left"],
                zIndex: 1,
              }}
            >
              SUMMER SALE
            </Text>

            <Text
              my={15}
              variant="ImageLabel1"
              sx={{
                fontSize: 40,
                textAlign: ["center", "center", "center", "left", "left"],
                zIndex: 1,
              }}
            >
              -20%
              <Text
                ml={15}
                variant="ChooseLabel"
                sx={{ letterSpacing: "2px", fontFamily: "sans-serif" }}
              >
                ON ALL CAKES
              </Text>
            </Text>

            <Text
              variant="ChooseContent"
              sx={{
                textAlign: ["center", "center", "center", "left", "left"],
              }}
            >
              Purchase our tasty cakes and sweets for your next event or family
              dinner at our online shop and save more money than anywhere.
            </Text>
            <Link href={`/products`} passHref>
              <Flex
                m="20px 0 60px"
                mb={[0, 0, 60, 60, 60]}
                sx={{
                  justifyContent: [
                    "center",
                    "center",
                    "center",
                    "left",
                    "left",
                  ],
                }}
              >
                <Button
                  p="25px 50px"
                  variant="secondary"
                  sx={{ boxShadow: "#bebebe 0px 5px 12px 0px" }}
                >
                  SHOP NOW
                </Button>
              </Flex>
            </Link>
          </Flex>
        </Flex>
      </Flex>
      {/* about-us session */}
      <Flex
        sx={{
          width: "100%",
          justifyContent: "center",
        }}
      >
        <Flex
          sx={{
            p: "60px 0 100px",
            width: [600, 600, 700, 1170, 1170],
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Text mb={60} mt={[0, 0, 60, 60, 60]} variant="offerLabel1">
            About Us
          </Text>
          <Grid columns={[1, 1, 1, 2, 2]} gap={0}>
            <Flex
              sx={{
                justifyContent: "center",
              }}
            >
              <Box
                px={15}
                sx={{
                  position: "relative",
                  height: [0, 0, 0, 484, 484],
                  width: [0, 0, 0, 555, 555],
                }}
              >
                <Image
                  src={about}
                  placeholder="blur"
                  alt="aboutUs"
                  layout="fill"
                />
              </Box>
            </Flex>

            <Flex
              px={15}
              sx={{
                flexDirection: "column",
              }}
            >
              <Flex
                mt={[40, 40, 40, 0, 0]}
                sx={{
                  p: "8% 4% 10% 18%",
                  flexDirection: "column",
                  position: "relative",
                  bg: "transparent",
                  zIndex: 1,
                  ":after": {
                    content: "''",
                    position: "absolute",
                    height: "calc(100% - 30px)",
                    border: "15px solid #f5f5f5",
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    width: "38%",
                  },
                }}
              >
                <Flex
                  sx={{
                    bg: "white",
                    flexDirection: "column",
                    py: 24,
                  }}
                >
                  <Text
                    variant="ChooseLabel"
                    sx={{
                      cursor: "pointer",
                      ":hover": {
                        color: "productType1",
                      },
                    }}
                  >
                    {AboutUsItems[page - 1].label}
                  </Text>
                  <Text
                    variant="ChooseContent"
                    sx={{
                      fontSize: [10, 12, 12, 14, 14],
                      height: 80,
                      overflow: "hidden",
                    }}
                  >
                    {AboutUsItems[page - 1].content}
                  </Text>
                </Flex>
                <Flex
                  sx={{
                    justifyContent: "center",
                  }}
                >
                  <Link href={AboutUsItems[page - 1].href}>
                    <Button
                      className="button"
                      p="12px 22px"
                      variant="OfferButton"
                      sx={{
                        ":hover": {
                          transform: "translateY(10px)",
                          opacity: 0.9,
                          ":before": {
                            transform: "scale(1)",
                            borderColor: "productType1",
                          },
                        },
                      }}
                    >
                      READ MORE
                    </Button>
                  </Link>
                </Flex>
              </Flex>
              <Flex
                mt={[40, 40, 40, "auto", "auto"]}
                bg="aboutUs"
                sx={{
                  justifyContent: "space-around",
                  width: "100%",
                  counterReset: "item",
                  alignItems: "center",
                }}
              >
                {AboutUsItems.map((item) => (
                  <Button
                    onClick={() => onPage(item.id)}
                    py={50}
                    m={0}
                    key={item.id}
                    bg="aboutUs"
                    variant="AboutUsButton"
                    sx={{
                      opacity: page === item.id ? 1 : 0.5,
                      color: page === item.id ? "productType1" : "productType2",
                      ":after": {
                        color:
                          page === item.id ? "productType1" : "productType2",
                      },
                      ":before": {
                        transform:
                          page === item.id
                            ? "translate(-50%, -100%)"
                            : "translateY(-50%)",
                      },
                    }}
                  >
                    {item.button}
                  </Button>
                ))}
              </Flex>
            </Flex>
          </Grid>
        </Flex>
      </Flex>
      {/* final session */}
      <Flex
        sx={{
          width: "100%",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            width: "100%",
            height: "100%",
            top: 0,
            left: 0,
          }}
        >
          <Box
            sx={{
              position: "relative",
              opacity: [0.3, 0.3, 0.3, 1, 1],
              width: "100%",
              height: "100%",
            }}
          >
            <Image
              src={partner}
              alt="partner"
              layout="fill"
              placeholder="blur"
              objectFit="cover"
            />
          </Box>
        </Box>
        <Flex
          sx={{
            py: [50, 50, 100, 100, 100],
            width: 1170,
            justifyContent: "space-around",
            alignItems: "center",
            overflowX: "auto",
          }}
        >
          <Box px={15}>
            <Image
              src={partner1}
              placeholder="blur"
              alt="images/partner-1.png"
              width={120}
              height={100}
            />
          </Box>
          <Box px={15}>
            <Image
              src={partner2}
              placeholder="blur"
              alt="images/partner-2.png"
              width={120}
              height={100}
            />
          </Box>
          <Box px={15}>
            <Image
              src={partner3}
              placeholder="blur"
              alt="images/partner-3.png"
              width={120}
              height={100}
            />
          </Box>
          <Box px={15}>
            <Image
              src={partner4}
              placeholder="blur"
              alt="images/partner-4.png"
              width={120}
              height={100}
            />
          </Box>
          <Box px={15}>
            <Image
              src={partner5}
              placeholder="blur"
              alt="images/partner-5.png"
              width={120}
              height={100}
            />
          </Box>
        </Flex>
      </Flex>
    </Layout>
  );
};

//SSG (load data on the server (nextjs server) before the page is displayed)
export const getStaticProps = getStaticPropsFunc(async ({ queryClient }) => {
  await prefetchQuery(queryClient, "/products");
  return {};
});

export default memo(index);
