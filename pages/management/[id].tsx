import { NextPage } from "next";
import Image from "next/image";
import React, { memo, useMemo } from "react";
import { ParsedUrlQuery } from "querystring";
import { OrderAPIProps } from "../../utils/interface";
import {
  Flex,
  Grid,
  Box,
  Text,
  Label,
  Input,
  Textarea,
  Button,
  Spinner,
} from "@theme-ui/components";
import { Cycle } from "../../components/ChooseUs";
import { fromImageToUrl } from "../../utils/urls";
import Link from "next/link";
import { formatCurrency, formatDate } from "../../utils/format";
import ListEmpty from "../../components/ListEmpty";
import cupcake from "../../public/images/cupcake.png";
import { CardProps } from "../../components/ProductCard";
import { CartIcon, InfoIcon } from "../../public/icon";
import AuthLayout from "../../containers/AuthLayout";
import { getServerSidePropsFunc, useFetchQuery } from "../../queries";

const detail: NextPage<{ id: string }> = ({ id }) => {
  const { isLoading, data: orderHistory } = useFetchQuery<OrderAPIProps>(
    `/orders/${id}`
  );
  const { isLoading: loading, data: products } =
    useFetchQuery<CardProps[]>("/products");

  const cart = useMemo(
    () =>
      products?.filter((product) =>
        orderHistory?.cart.find((item) => item.product === product.id)
      ),
    [orderHistory, products]
  );
  const quantity = useMemo(
    () => orderHistory?.cart?.reduce((sum, item) => sum + item.quantity, 0),
    [orderHistory, products]
  );

  return (
    <AuthLayout>
      <Flex
        sx={{
          bg: "white",
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Text
          sx={{
            pt: [30, 30, 60, 60, 60],
            pb: [0, 0, 30, 30, 30],
            width: "100%",
            maxWidth: 1170,
            fontSize: [40, 40, 50, 50, 50],
            fontWeight: [700, 700, 600, 600, 600],
            textTransform: "uppercase",
            letterSpacing: "1.5px",
            textAlign: "center",
          }}
        >
          Order History
        </Text>
        <Grid
          columns={[1, 1, "3fr 2fr", "3fr 2fr", "3fr 2fr"]}
          gap={0}
          sx={{
            py: 20,
            width: "100%",
            maxWidth: 1170,
            flexDirection: "column",
          }}
        >
          <Flex
            pt={30}
            sx={{
              flexDirection: "column",
              ":hover": {
                ".icon": {
                  transform: "rotate(-0.15turn)",
                },
              },
            }}
          >
            <Flex
              sx={{
                px: 15,
                alignItems: "center",
              }}
            >
              <Flex
                sx={{
                  position: "relative",
                  justifyContent: "center",
                }}
              >
                <Box
                  sx={{
                    m: "2px 13px 0px",
                    width: 50,
                    pl: 2,
                    svg: { fill: "productType1" },
                  }}
                >
                  <CartIcon width={40} height={40} />
                </Box>
                <Flex
                  className="icon"
                  sx={{
                    position: "absolute",
                    top: -10,
                    transition: "0.3s",
                  }}
                >
                  <Cycle
                    width={70}
                    height={70}
                    opacity={0.07}
                    transform="rotate(90)"
                  />
                </Flex>
              </Flex>
              <Text
                px={15}
                my={15}
                sx={{
                  fontSize: [25, 25, 30, 30, 30],
                  fontWeight: 700,
                  letterSpacing: "1px",
                  color: "productType1",
                }}
              >
                Shopping Cart
              </Text>
            </Flex>

            <Grid
              columns={"10fr 6fr 7fr"}
              gap={[10, 10, 30, 30, 30]}
              p={15}
              mt={15}
              sx={{
                borderBottom: "1px solid #bebebe",
              }}
            >
              <Text variant="cartItemPrice">Product</Text>
              <Text
                variant="cartItemPrice"
                sx={{
                  textAlign: "center",
                }}
              >
                Quantity
              </Text>
              <Text
                variant="cartItemPrice"
                sx={{
                  px: [10, 10, 20, 20, 20],
                  textAlign: "end",
                }}
              >
                Total
              </Text>
            </Grid>
            <Flex
              px={15}
              sx={{
                maxHeight: 350,
                minHeight: [160, 160, 350, 350, 350],
                flexDirection: "column",
                overflowY: "auto",
                "::-webkit-scrollbar": {
                  width: 7,
                },
                "::-webkit-scrollbar-thumb": {
                  bg: "productType1",
                  borderRadius: 5,
                },
              }}
            >
              {cart?.length ? (
                cart.map(
                  (item) =>
                    item && (
                      <Grid
                        columns={"10fr 6fr 7fr"}
                        gap={[10, 10, 30, 30, 30]}
                        py={15}
                        key={item?.id}
                        sx={{
                          alignItems: "center",
                          borderBottom: "1px solid #e6e6e6",
                        }}
                      >
                        <Grid
                          columns={"50px 1fr"}
                          gap={0}
                          sx={{
                            alignItems: "center",
                          }}
                        >
                          <Flex
                            sx={{
                              width: 50,
                              height: 50,
                              borderRadius: 5,
                              border: "1px solid #d3d1d1",
                              overflow: "hidden",
                            }}
                          >
                            <Box
                              p={2}
                              sx={{
                                transition: "0.3s",
                                ":hover": {
                                  transform: "scale(1.1)",
                                },
                              }}
                            >
                              <Image
                                src={fromImageToUrl(item?.image)}
                                alt="Product"
                                width={50}
                                height={50}
                              />
                            </Box>
                          </Flex>

                          <Link href={`/products/${item?.id}`} passHref>
                            <Text
                              px={15}
                              variant="cartItemLabel"
                              sx={{
                                whiteSpace: "nowrap",
                                overflowX: "hidden",
                                textOverflow: "ellipsis",
                              }}
                            >
                              {item?.name}
                            </Text>
                          </Link>
                        </Grid>

                        <Text
                          variant="cartItemQuantity"
                          my="auto"
                          sx={{
                            fontSize: 16,
                            width: "100%",
                            textAlign: "center",
                          }}
                        >
                          {
                            orderHistory?.cart.find(
                              (x) => x.product === item.id
                            )?.quantity
                          }
                        </Text>

                        <Text
                          variant="cartItemPrice"
                          sx={{
                            textAlign: "end",
                          }}
                        >
                          {formatCurrency(
                            item?.price *
                              orderHistory?.cart.find(
                                (x) => x.product === item.id
                              )?.quantity
                          )}
                          <Text as="sup">đ</Text>
                        </Text>
                      </Grid>
                    )
                )
              ) : isLoading || loading ? (
                <Flex
                  sx={{
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <Spinner my={80} color="productType1" />
                </Flex>
              ) : (
                <Flex
                  sx={{
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <ListEmpty />
                </Flex>
              )}
            </Flex>
            <Flex>
              <Flex
                sx={{
                  borderTop: "1px solid #d3d1d1",
                  p: 15,
                  width: "100%",
                  flexDirection: "column",
                }}
              >
                <Grid columns={2}>
                  <Text variant="cartLabel">Quantity: </Text>
                  <Text
                    variant="cartLabel"
                    sx={{
                      textAlign: "end",
                    }}
                  >
                    {quantity} PRODUCTS
                  </Text>
                </Grid>
                <Grid
                  columns={2}
                  sx={{
                    alignItems: "end",
                  }}
                >
                  <Text variant="cartLabel" color="productType2">
                    Delivery charges:
                  </Text>
                  <Text
                    color="productType2"
                    variant="cartLabel"
                    sx={{
                      textAlign: "end",
                    }}
                  >
                    0<Text as="sup">Đ</Text>
                  </Text>
                </Grid>
                <Grid
                  columns={2}
                  sx={{
                    alignItems: "end",
                  }}
                >
                  <Text
                    variant="cartLabel1"
                    sx={{
                      fontWeight: 700,
                    }}
                  >
                    TOTAL:
                  </Text>
                  <Text
                    variant="cartLabel1"
                    sx={{
                      textAlign: "end",
                      fontWeight: 700,
                    }}
                  >
                    {formatCurrency(orderHistory?.total)}
                    <Text as="sup">Đ</Text>
                  </Text>
                </Grid>
              </Flex>
            </Flex>
          </Flex>
          <Flex
            bg="productType1"
            sx={{
              pt: 30,
              mx: 15,
              borderRadius: 15,
              flexDirection: "column",
              overflowX: "hidden",
              ":hover": {
                ".icon": {
                  transform: "rotate(0.15turn)",
                },
              },
            }}
          >
            <Flex
              sx={{
                px: 15,
                alignItems: "center",
                justifyContent: "center",
                transform: "translateX(15px)",
              }}
            >
              <Text
                px={15}
                my={15}
                sx={{
                  fontSize: [25, 25, 30, 30, 30],
                  fontWeight: 700,
                  letterSpacing: "1px",
                  color: "white",
                }}
              >
                Information
              </Text>
              <Flex
                sx={{
                  position: "relative",
                  justifyContent: "center",
                }}
              >
                <Box
                  sx={{
                    m: "2px 13px 0",
                    width: 50,
                    pl: 2,
                    svg: { fill: "white" },
                  }}
                >
                  <InfoIcon width={40} height={40} />
                </Box>
                <Flex
                  className="icon"
                  sx={{
                    position: "absolute",
                    top: -10,
                    transition: "0.3s",
                  }}
                >
                  <Cycle width={70} height={70} fill="white" opacity={0.15} />
                </Flex>
              </Flex>
            </Flex>
            <Flex
              pt={20}
              sx={{
                flexDirection: "column",
                width: "100%",
                overflow: "hidden",
              }}
            >
              {/* Name and phone */}
              <Grid columns={2} gap={0}>
                <Box>
                  <Box m="10px 0 10px 20px">
                    <Label
                      sx={{
                        color: "white",
                        letterSpacing: "1px",
                        fontSize: 14,
                        fontWeight: 700,
                      }}
                    >
                      Name
                    </Label>
                    <Input
                      sx={{
                        p: "10px 15px",
                        transition: "0.3s",
                        borderRadius: 30,
                        borderColor: "loginBorder",
                        bg: "white",
                        "::placeholder": {
                          fontSize: 14,
                          opacity: 0.5,
                        },
                      }}
                      defaultValue={orderHistory?.name}
                      disabled={true}
                      placeholder="Your name"
                    />
                  </Box>

                  <Box m="10px 0 10px 20px">
                    <Label
                      sx={{
                        color: "white",
                        letterSpacing: "1px",
                        fontSize: 14,
                        fontWeight: 700,
                      }}
                    >
                      Phone
                    </Label>
                    <Input
                      sx={{
                        p: "10px 15px",
                        transition: "0.3s",
                        borderRadius: 30,
                        borderColor: "loginBorder",
                        bg: "white",
                        "::placeholder": {
                          fontSize: 14,
                          opacity: 0.5,
                        },
                      }}
                      defaultValue={orderHistory?.phone}
                      disabled={true}
                      placeholder="Your phone"
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    m: "auto",
                    p: 20,
                    width: 150,
                    height: 150,
                    bg: "white",
                    opacity: 0.8,
                    borderRadius: 99,
                    textAlign: "center",
                  }}
                >
                  <Box>
                    <Image
                      src={cupcake}
                      placeholder="blur"
                      width={80}
                      height={80}
                      alt="/images/cupcake.png"
                    />
                  </Box>
                  <Text
                    sx={{
                      fontSize: 18,
                      fontWeight: 700,
                      color: "productType1",
                    }}
                  >
                    Thank you!
                  </Text>
                </Box>
              </Grid>
              {/* Address */}
              <Box m="5px 20px 10px">
                <Label
                  sx={{
                    color: "white",
                    letterSpacing: "1px",
                    fontSize: 14,
                    fontWeight: 700,
                  }}
                >
                  Address
                </Label>
                <Input
                  sx={{
                    p: "10px 15px",
                    transition: "0.3s",
                    borderRadius: 30,
                    borderColor: "loginBorder",
                    bg: "white",
                    "::placeholder": {
                      fontSize: 14,
                      opacity: 0.5,
                    },
                  }}
                  defaultValue={orderHistory?.address}
                  disabled={true}
                  placeholder="Address"
                />
              </Box>
              {/* Note */}
              <Box m="5px 20px 10px">
                <Label
                  sx={{
                    color: "white",
                    letterSpacing: "1px",
                    fontSize: 14,
                    fontWeight: 700,
                  }}
                >
                  Note
                </Label>
                <Textarea
                  mb={30}
                  rows={10}
                  sx={{
                    p: "10px 15px",
                    transition: "0.3s",
                    maxHeight: 150,
                    borderRadius: 20,
                    borderColor: "loginBorder",
                    fontSize: 16,
                    fontFamily: "sans-serif",
                    bg: "white",
                    "::placeholder": {
                      fontSize: 14,
                      opacity: 0.5,
                    },
                  }}
                  defaultValue={orderHistory?.note}
                  disabled={true}
                  placeholder="Note"
                />
              </Box>
            </Flex>
          </Flex>
        </Grid>
        <Flex
          sx={{
            py: ["5px", "5px", 15, 15, 15],
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text
            as="i"
            sx={{
              mx: 15,
              color: "productType2",
              opacity: 0.8,
              fontSize: 14,
            }}
          >
            * This your order was created at{" "}
            {formatDate(orderHistory?.createdAt)}
          </Text>
        </Flex>
        <Flex
          sx={{
            py: 35,
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid
            columns={["3fr 2fr", "2fr 2fr", "3fr 4fr", "2fr 4fr", "2fr 4fr"]}
            gap={0}
            sx={{
              mx: 15,
              width: 1170,
              alignItems: "center",
            }}
          >
            <Text
              px={15}
              sx={{
                color: "productPriceOld",
                letterSpacing: "1px",
                fontSize: [14, 14, 16, 16, 16],
                fontWeight: 500,
                opacity: 0.7,
              }}
            >
              Do you want to continue shopping?
            </Text>
            <Flex>
              <Link href="/products" passHref>
                <Button
                  sx={{
                    p: [
                      "15px 20px",
                      "15px 20px",
                      "15px 30px",
                      "15px 30px",
                      "15px 30px",
                    ],
                    borderRadius: 99,
                    fontSize: [12, 12, 16, 16, 16],
                  }}
                >
                  Go Shopping
                </Button>
              </Link>
            </Flex>
          </Grid>
        </Flex>
      </Flex>
    </AuthLayout>
  );
};

interface Params extends ParsedUrlQuery {
  id: string;
}

export const getServerSideProps = getServerSidePropsFunc(async ({ params }) => {
  const { id } = params as Params;
  return {
    id,
  };
});

export default memo(detail);
