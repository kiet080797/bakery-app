import type { NextApiRequest, NextApiResponse } from "next";
import Cookies from "cookies";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method !== "GET") {
    return res.status(404).json({ message: "method is not supported" });
  }

  const cookies = new Cookies(req, res);
  const accessToken = cookies.get("access_token");
  if (accessToken) {
    res.status(200).json({ isLogin: true });
  } else {
    res.status(200).json({ isLogin: false });
  }
}
