import type { NextApiRequest, NextApiResponse } from "next";
import Cookies from "cookies";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method !== "POST") {
    return res.status(404).json({ message: "method is not supported" });
  }

  const cookies = new Cookies(req, res);
  cookies.set("access_token");

  res.status(200).json({ message: "logout successfully" });
}
