import {
  Box,
  Button,
  Field,
  Flex,
  Grid,
  Text,
  IconButton,
  Label,
  Input,
  Message,
  Spinner,
} from "@theme-ui/components";
import Image from "next/image";
import { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { memo, useCallback, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import logo from "../public/images/logo.png";
import loginImage from "../public/images/bg-cta.jpg";
import { LoginPayload } from "../models";
import { EyeIcon } from "../public/icon";
import { useFetchMutation, useLoginMutation } from "../queries";

const login: NextPage = () => {
  //use react-hook-form to validate login form
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginPayload>({
    defaultValues: {
      email: "tien1997@gmail.com",
      password: "123456",
    },
  });
  const router = useRouter();
  const [isShowPassword, setIsShowPassword] = useState<boolean>(false);
  const [isLogin, setIsLogin] = useState<boolean>(false);
  //check logged in
  const { mutate: checkAuth } = useFetchMutation<{ isLogin: boolean }>();
  //login API
  const { isLoading, mutate, isSuccess } = useLoginMutation();
  const [isError, setIsError] = useState<boolean>(false);

  const toggle = useCallback(() => setIsShowPassword((prev) => !prev), []);

  //handle check logged in before page render
  useEffect(() => {
    checkAuth("/checkAuth", {
      onSuccess: async (data) => {
        if (data.isLogin) {
          setIsLogin(true);
        }
      },
    });
  }, []);
  //handle login
  const onSubmit = useCallback((data: LoginPayload) => {
    mutate(data, {
      onSuccess: () => {
        router.push("/", undefined, { shallow: true });
      },
      onError: () => setIsError(true),
    });
  }, []);

  const onCloseMessage = useCallback(() => {
    setIsError(false);
    setIsLogin(false);
  }, []);

  return (
    <Grid
      onClick={onCloseMessage}
      columns={[1, 1, 1, "3fr 5fr", "3fr 5fr"]}
      sx={{
        width: "100%",
        height: "100vh",
        gap: 0,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <Head>
        <title>Sweet Bakery</title>
        <link rel="shortcut icon" href="/images/favicon.ico" />
        <meta property="og:title" content="Bakery" />
        <meta
          property="og:image"
          content="https://toppng.com/uploads/preview/51-bakery-icon-packs-dessert-pie-icon-115552356724bznqtsuv1.png"
        />
        <meta property="og:image:alt" content="bakery" />
        <meta
          name="description"
          content="Specializes in selling all kinds of wholesale and retail cakes"
        />
        <meta
          property="og:description"
          content="Specializes in selling all kinds of wholesale and retail cakes"
        />
      </Head>
      <Flex
        sx={{
          width: "100%",
          height: "100%",
          alignItems: [
            "flex-start",
            "flex-start",
            "flex-start",
            "center",
            "center",
          ],
          justifyContent: "center",
          bg: "white",
        }}
      >
        <Flex
          sx={{
            mx: 30,
            width: 360,
            zIndex: 1,
            flexDirection: "column",
          }}
        >
          <Link href="/" passHref>
            <Box
              sx={{
                pt: [50, 50, 50, 0, 0],
                cursor: "pointer",
                textAlign: ["center", "center", "center", "left", "left"],
              }}
            >
              <Image
                width={170}
                height={40}
                src={logo}
                placeholder="blur"
                alt="images/logo.png"
              />
            </Box>
          </Link>
          <Text
            sx={{
              fontSize: 40,
              fontWeight: 700,
              letterSpacing: "2px",
              textAlign: ["center", "center", "center", "left", "left"],
            }}
            m="30px 0 15px"
          >
            Welcome Back
          </Text>
          <Text
            sx={{
              fontSize: 24,
              letterSpacing: "2px",
              opacity: 0.8,
              textAlign: ["center", "center", "center", "left", "left"],
            }}
            mb={40}
          >
            Log into your account
          </Text>

          <Flex
            as="form"
            sx={{ flexDirection: "column", width: "100%" }}
            onSubmit={handleSubmit(onSubmit)}
          >
            <Box mb={20}>
              <Field
                sx={{
                  p: "10px 15px",
                  transition: "0.3s",
                  borderColor: "loginBorder",
                  "::placeholder": {
                    opacity: 0.5,
                  },
                  ":focus": {
                    outline: "none",
                    borderColor: "productType5",
                  },
                }}
                type="email"
                label="Email"
                {...register("email", {
                  required: "Please, enter your email!",
                })}
                placeholder="Email"
              />
              {errors.email && (
                <Text variant="errorLogin">{errors.email.message}</Text>
              )}
            </Box>
            <Box mb={20}>
              <Box>
                <Label>Password</Label>
                <Flex
                  sx={{
                    position: "relative",
                    alignItems: "center",
                  }}
                >
                  <Input
                    sx={{
                      p: "10px 15px",
                      transition: "0.3s",
                      borderColor: "loginBorder",
                      "::placeholder": {
                        opacity: 0.5,
                      },
                      ":focus": {
                        outline: "none",
                        borderColor: "productType5",
                      },
                    }}
                    type={isShowPassword ? "text" : "password"}
                    {...register("password", {
                      required: "Please, enter password!",
                    })}
                    placeholder="Password"
                  />
                  <IconButton
                    type="button"
                    color="lightGray1"
                    onClick={toggle}
                    sx={{
                      position: "absolute",
                      right: 0,
                      ":hover": {
                        color: "text",
                        transition: "0.3s",
                      },
                      cursor: "pointer",
                    }}
                  >
                    <EyeIcon isShow={isShowPassword} />
                  </IconButton>
                </Flex>
              </Box>
              {errors.password && (
                <Text variant="errorLogin">{errors.password.message}</Text>
              )}
            </Box>
            {isError && (
              <Message mb={15} variant="error">
                Your email or password is incorrect
              </Message>
            )}
            {isLoading && (
              <Flex
                mb={15}
                sx={{
                  justifyContent: "center",
                }}
              >
                <Spinner variant="styles.spinner" />
              </Flex>
            )}
            {isSuccess && (
              <Message mb={15} variant="success">
                Successful login
              </Message>
            )}
            {isLogin && (
              <Message mb={15} variant="error">
                You must log out of your old account
              </Message>
            )}

            <Flex>
              <Text variant="signUp">Do you not have an account? </Text>
              <Link href="/register" passHref>
                <Text
                  px={3}
                  variant="signUp"
                  sx={{
                    fontWeight: 600,
                    cursor: "pointer",
                    transition: "0.3s",
                    ":hover": {
                      color: "productType1",
                    },
                  }}
                >
                  Register
                </Text>
              </Link>
            </Flex>

            <Flex mt={20}>
              <Button
                disabled={isLoading || isLogin}
                p="10px 50px"
                variant="secondary"
                type="submit"
                sx={{
                  width: ["100%", "100%", "100%", "auto", "auto"],
                }}
              >
                Sign in
              </Button>
            </Flex>
          </Flex>
        </Flex>
      </Flex>
      <Box
        sx={{
          position: "fixed",
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          zIndex: -1,
        }}
      >
        <Box
          sx={{
            position: "relative",
            visibility: ["hidden", "hidden", "hidden", "visible", "visible"],
            width: "100%",
            height: "100%",
          }}
        >
          <Image
            src={loginImage}
            layout="fill"
            placeholder="blur"
            alt="login"
          />
        </Box>
      </Box>
    </Grid>
  );
};

export default memo(login);
