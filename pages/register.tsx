import {
  Box,
  Button,
  Field,
  Flex,
  Grid,
  Text,
  IconButton,
  Label,
  Input,
  Message,
  Spinner,
} from "@theme-ui/components";
import { useRouter } from "next/router";
import Image from "next/image";
import { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";

import React, {
  Dispatch,
  memo,
  SetStateAction,
  useCallback,
  useRef,
  useState,
} from "react";
import { useForm } from "react-hook-form";
import { EyeIcon } from "../public/icon";
import { RegisterPayload } from "../models";
import { useRegisterMutation } from "../queries";
import Logo from "../public/images/logo.png";
import RegisterImage from "../public/images/bg-hero.jpg";

const register: NextPage = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm<RegisterPayload>({});
  const password = useRef({});
  password.current = watch("password", "");

  const { isLoading, mutate, isSuccess } = useRegisterMutation();
  const [isError, setIsError] = useState<boolean>(false);

  const router = useRouter();

  const [isShowPassword, setIsShowPassword] = useState<boolean>(false);
  const [isShowConfirm, setIsShowConfirm] = useState<boolean>(false);

  const toggle = useCallback(
    (x: Dispatch<SetStateAction<boolean>>) => x((prev) => !prev),
    []
  );

  const onSubmit = useCallback((data: RegisterPayload) => {
    mutate(data, {
      onSuccess: async () => {
        setTimeout(() => {
          router.push("/login", undefined, { shallow: true });
        }, 1000);
      },
      onError: () => setIsError(true),
    });
  }, []);

  const onCloseMessage = useCallback(() => {
    setIsError(false);
  }, []);

  return (
    <Grid
      onClick={onCloseMessage}
      columns={[1, 1, 1, "3fr 5fr", "3fr 5fr"]}
      sx={{
        width: "100%",
        height: "100vh",
        gap: 0,
        bg: "white",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <Head>
        <title>Sweet Bakery</title>
        <link rel="shortcut icon" href="/images/favicon.ico" />
        <meta property="og:title" content="Bakery" />
        <meta
          property="og:image"
          content="https://toppng.com/uploads/preview/51-bakery-icon-packs-dessert-pie-icon-115552356724bznqtsuv1.png"
        />
        <meta property="og:image:alt" content="bakery" />
        <meta
          name="description"
          content="Specializes in selling all kinds of wholesale and retail cakes"
        />
        <meta
          property="og:description"
          content="Specializes in selling all kinds of wholesale and retail cakes"
        />
      </Head>
      <Flex
        sx={{
          bg: "white",
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Flex
          sx={{
            m: "0 30px 30px",
            width: 360,
            flexDirection: "column",
          }}
        >
          <Link href="/" passHref>
            <Box
              sx={{
                my: 20,
                cursor: "pointer",
                textAlign: ["center", "center", "center", "left", "left"],
              }}
            >
              <Image
                src={Logo}
                placeholder="blur"
                alt="/images/logo.png"
                width={170}
                height={40}
              />
            </Box>
          </Link>
          <Text
            sx={{
              fontSize: 30,
              fontWeight: 700,
              letterSpacing: "1px",
              textAlign: ["center", "center", "center", "left", "left"],
            }}
            mb={25}
          >
            Create an account
          </Text>

          <Flex
            as="form"
            sx={{ flexDirection: "column", width: "100%" }}
            onSubmit={handleSubmit(onSubmit)}
          >
            <Grid mb={15} columns={2} gap={15}>
              <Box>
                <Field
                  sx={{
                    p: "10px 15px",
                    transition: "0.3s",
                    borderColor: "loginBorder",
                    "::placeholder": {
                      opacity: 0.5,
                    },
                    ":focus": {
                      outline: "none",
                      borderColor: "productType5",
                    },
                  }}
                  label="First name"
                  {...register("firstName", {
                    required: "Enter your first name!",
                  })}
                  placeholder="First name"
                />
                {errors.firstName && (
                  <Text variant="errorLogin">{errors.firstName.message}</Text>
                )}
              </Box>
              <Box>
                <Field
                  sx={{
                    p: "10px 15px",
                    transition: "0.3s",
                    borderColor: "loginBorder",
                    "::placeholder": {
                      opacity: 0.5,
                    },
                    ":focus": {
                      outline: "none",
                      borderColor: "productType5",
                    },
                  }}
                  label="Last name"
                  {...register("lastName", {
                    required: "Enter your last name!",
                  })}
                  placeholder="Last name"
                />
                {errors.lastName && (
                  <Text variant="errorLogin">{errors.lastName.message}</Text>
                )}
              </Box>
            </Grid>
            <Box mb={15}>
              <Field
                sx={{
                  p: "10px 15px",
                  transition: "0.3s",
                  borderColor: "loginBorder",
                  "::placeholder": {
                    opacity: 0.5,
                  },
                  ":focus": {
                    outline: "none",
                    borderColor: "productType5",
                  },
                }}
                type="email"
                label="Email"
                {...register("email", {
                  required: "Please, enter your email!",
                })}
                placeholder="Email"
              />
              {errors.email && (
                <Text variant="errorLogin">{errors.email.message}</Text>
              )}
            </Box>
            <Box mb={15}>
              <Field
                sx={{
                  p: "10px 15px",
                  transition: "0.3s",
                  borderColor: "loginBorder",
                  "::placeholder": {
                    opacity: 0.5,
                  },
                  ":focus": {
                    outline: "none",
                    borderColor: "productType5",
                  },
                }}
                label="Username"
                {...register("username", {
                  required: "Please, enter your username!",
                })}
                placeholder="Username"
              />
              {errors.username && (
                <Text variant="errorLogin">{errors.username.message}</Text>
              )}
            </Box>
            <Box mb={15}>
              <Box>
                <Label>Password</Label>
                <Flex
                  sx={{
                    position: "relative",
                    alignItems: "center",
                  }}
                >
                  <Input
                    sx={{
                      p: "10px 15px",
                      transition: "0.3s",
                      borderColor: "loginBorder",
                      "::placeholder": {
                        opacity: 0.5,
                      },
                      ":focus": {
                        outline: "none",
                        borderColor: "productType5",
                      },
                    }}
                    type={isShowPassword ? "text" : "password"}
                    {...register("password", {
                      required: "Please, enter password!",
                      minLength: {
                        value: 6,
                        message: "Password must have at least 6 characters",
                      },
                    })}
                    placeholder="Password"
                  />
                  <IconButton
                    type="button"
                    color="lightGray1"
                    onClick={() => toggle(setIsShowPassword)}
                    sx={{
                      position: "absolute",
                      right: 0,
                      ":hover": {
                        color: "text",
                        transition: "0.3s",
                      },
                      cursor: "pointer",
                    }}
                  >
                    <EyeIcon isShow={isShowPassword} />
                  </IconButton>
                </Flex>
              </Box>
              {errors.password && (
                <Text variant="errorLogin">{errors.password.message}</Text>
              )}
            </Box>
            <Box mb={15}>
              <Box>
                <Label>Confirm password</Label>
                <Flex
                  sx={{
                    position: "relative",
                    alignItems: "center",
                  }}
                >
                  <Input
                    as="input"
                    sx={{
                      p: "10px 15px",
                      transition: "0.3s",
                      borderColor: "loginBorder",
                      "::placeholder": {
                        opacity: 0.5,
                      },
                      ":focus": {
                        outline: "none",
                        borderColor: "productType5",
                      },
                    }}
                    type={isShowConfirm ? "text" : "password"}
                    {...register("confirmPassword", {
                      required: "Please, enter confirm password!",
                      validate: (value) =>
                        value === password.current ||
                        "The passwords do not match",
                    })}
                    placeholder="Confirm password"
                  />
                  <IconButton
                    type="button"
                    color="lightGray1"
                    onClick={() => toggle(setIsShowConfirm)}
                    sx={{
                      position: "absolute",
                      right: 0,
                      ":hover": {
                        color: "text",
                        transition: "0.3s",
                      },
                      cursor: "pointer",
                    }}
                  >
                    <EyeIcon isShow={isShowConfirm} />
                  </IconButton>
                </Flex>
              </Box>
              {errors.confirmPassword && (
                <Text variant="errorLogin">
                  {errors.confirmPassword.message}
                </Text>
              )}
            </Box>
            {isError && (
              <Message mb={15} variant="error">
                Your email or username already exists
              </Message>
            )}
            {isSuccess && (
              <Message mb={15} variant="success">
                You have successfully registered
              </Message>
            )}
            {isLoading && (
              <Flex
                mb={15}
                sx={{
                  justifyContent: "center",
                }}
              >
                <Spinner variant="styles.spinner" />
              </Flex>
            )}

            <Flex>
              <Text variant="signUp">Do you already have an account? </Text>
              <Link href="/login" passHref>
                <Text
                  px={3}
                  variant="signUp"
                  sx={{
                    fontWeight: 600,
                    cursor: "pointer",
                    transition: "0.3s",
                    ":hover": {
                      color: "productType1",
                    },
                  }}
                >
                  Login
                </Text>
              </Link>
            </Flex>

            <Flex mt={20}>
              <Button
                p="10px 50px"
                variant="secondary"
                type="submit"
                disabled={isLoading}
                sx={{
                  width: ["100%", "100%", "100%", "auto", "auto"],
                }}
              >
                Register
              </Button>
            </Flex>
          </Flex>
        </Flex>
      </Flex>
      <Box
        sx={{
          position: "relative",
          visibility: ["hidden", "hidden", "hidden", "visible", "visible"],
          width: "100%",
          height: "100%",
        }}
      >
        <Image
          src={RegisterImage}
          layout="fill"
          placeholder="blur"
          alt="signUp"
          objectFit="fill"
        />
      </Box>
    </Grid>
  );
};

export default memo(register);
