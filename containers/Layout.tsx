import Head from "next/head";
import React, { FC, memo, PropsWithChildren, ReactNode } from "react";
import { Box, Flex } from "theme-ui";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Navigation from "../components/Navigation";

const Layout: FC<PropsWithChildren<ReactNode>> = ({ children }) => {
  return (
    <Flex
      bg="white"
      sx={{
        width: "100%",
        flexDirection: "column",
      }}
    >
      <Head>
        <title>Sweet Bakery</title>
        <link rel="shortcut icon" href="/images/favicon.ico" />
        <meta property="og:title" content="Bakery" />
        <meta
          property="og:image"
          content="https://res.cloudinary.com/dtd8ranav/image/upload/v1634816197/bg_hero_21562d49b5.jpg"
        />
        <meta property="og:image:alt" content="bakery" />
        <meta
          name="description"
          content="Specializes in selling all kinds of wholesale and retail cakes"
        />
        <meta
          property="og:description"
          content="Specializes in selling all kinds of wholesale and retail cakes"
        />
      </Head>
      <Header />
      <Navigation />
      <Box>{children}</Box>
      <Footer />
    </Flex>
  );
};

export default memo(Layout);
