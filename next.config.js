module.exports = {
  reactStrictMode: false,
  images: {
    domains: ["res.cloudinary.com"],
  },
  i18n: {
    locales: ["en-US"],
    defaultLocale: "en-US",
  },
};
