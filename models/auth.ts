export interface LoginPayload {
  email: string;
  password: string;
}

export interface RegisterPayload {
  lastName: string;
  firstName: string;
  username: string;
  password: string;
  email: string;
  confirmPassword: string;
}

export interface Profile {
  lastName: string;
  firstName: string;
  email: string;
  username: string;
  password: string;
  createdAt: Date;
}
