import { useCallback } from "react";
import useLocalStorage, { SetValue } from "./useLocalStorage";

export interface ProductLocal {
  id: string;
  quantity: number;
}

const useCart = (): [
  ProductLocal[],
  (id: string) => void,
  (id: string) => void,
  (id: string) => void,
  SetValue<ProductLocal[]>
] => {
  const [cart, setCart] = useLocalStorage<ProductLocal[]>("cart", []);
  const addToCart = useCallback(
    (id: string) => {
      const product = cart.find((item) => item.id === id);
      if (product) {
        product.quantity += 1;
      } else {
        cart.push({ id, quantity: 1 });
      }
      setCart(cart);
    },
    [cart]
  );

  const removeToCart = useCallback(
    (id: string) => {
      const product = cart.find((item) => item.id === id);
      if (product)
        if (product.quantity === 1) {
          removeAll(id);
        } else {
          product.quantity -= 1;
          setCart(cart);
        }
    },
    [cart]
  );

  const removeAll = useCallback(
    (id: string) => {
      const filter = cart.filter((item) => item.id !== id);
      setCart(filter);
    },
    [cart]
  );

  return [cart, addToCart, removeToCart, removeAll, setCart];
};

export default useCart;
