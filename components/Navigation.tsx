import { Flex, Spinner, Text } from "@theme-ui/components";
import React, {
  FC,
  memo,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import Search from "./Search";
import Cart from "./Cart";
import Link from "next/link";
import useOnClickOutside from "../hooks/useOnClickOutside";
import { useRouter } from "next/router";
import {
  logout,
  useFetchMutation,
  useFetchQuery,
  useProfileQuery,
} from "../queries";
import { LoginIcon } from "../public/icon";

const colors: string[] = [
  "#00aefd",
  "#ffa400",
  "#07a787",
  "#ff7870",
  "black",
  "pink",
  "yellow",
  "#e74c3c",
  "#2979ff",
];
const randomColor = colors[Math.floor(Math.random() * colors.length)];

const Navigation: FC = () => {
  const ref = useRef<HTMLDivElement | null>(null);
  useOnClickOutside(ref, () => setIsOpenPopup(false));
  const [isOpenPopup, setIsOpenPopup] = useState<boolean>(false);
  const { data, mutate: checkAuth } = useFetchMutation<{ isLogin: boolean }>();
  const { data: profile, isLoading } = useProfileQuery(!!data?.isLogin);

  useEffect(() => {
    checkAuth("/checkAuth");
  }, []);

  const router = useRouter();

  const onOpenPopup = useCallback(() => {
    setIsOpenPopup(!isOpenPopup);
  }, [isOpenPopup]);

  const onLogout = useCallback(async () => {
    try {
      await logout();
      router.push("/login", undefined, { shallow: true });
    } catch (error) {
      console.log("failed to logout");
    }
  }, []);

  return (
    <Flex
      sx={{
        zIndex: 98,
        height: 70,
        alignItems: "center",
        position: "sticky",
        top: 0,
        left: 0,
        boxShadow: "0 3px 5px #c5c1c1",
      }}
    >
      <Flex
        bg="white"
        sx={{
          position: "relative",
          width: "100%",
          mx: "auto",
          justifyContent: "center",
        }}
      >
        <Flex
          sx={{
            width: "100%",
            maxWidth: 1170,
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Search
            sx={{
              flex: 1,
              zIndex: 99,
            }}
          />
          {router.pathname !== "/management" && <Cart />}
          {data?.isLogin ? (
            <Flex
              ref={ref}
              sx={{
                position: "relative",
                transition: "0.3s",
                height: "100%",
                cursor: "pointer",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Flex
                onClick={onOpenPopup}
                sx={{
                  px: 15,
                  height: 70,
                  overflow: "hidden",
                  color: "productType2",
                  userSelect: "none",
                  alignItems: "center",
                  ":hover": {
                    color: "productType1",
                  },
                }}
              >
                <Flex
                  sx={{
                    bg: `${randomColor}`,
                    width: 35,
                    height: 35,
                    borderRadius: 999,
                    alignItems: "center",
                    justifyContent: "center",
                    transition: "0.3s",
                  }}
                >
                  {!isLoading ? (
                    <Text
                      sx={{
                        fontSize: 16,
                        color: "white",
                        fontWeight: 700,
                        textTransform: "uppercase",
                      }}
                    >
                      {profile?.firstName[0]}
                    </Text>
                  ) : (
                    <Spinner color="white" size={20} />
                  )}
                </Flex>
              </Flex>
              <Flex
                sx={{
                  bg: "white",
                  position: "absolute",
                  flexDirection: "column",
                  borderRadius: 3,
                  overflow: "hidden",
                  top: "100%",
                  right: 0,
                  width: 100,
                  boxShadow: "#c9c9c9 0px 5px 10px 0px",
                  textAlign: "center",
                  visibility: isOpenPopup ? "visible" : "hidden",
                  opacity: isOpenPopup ? 1 : 0,
                  transform: isOpenPopup ? "translateY(0)" : "translateY(30px)",
                  transition: "0.3s",
                }}
              >
                <Link href="/management/profile" passHref>
                  <Text
                    p="8px 15px 10px"
                    variant="MenuLabel"
                    sx={{
                      opacity: 0.8,
                      borderBottom: "1px solid #e2e2e2",
                    }}
                  >
                    Profile
                  </Text>
                </Link>
                <Text
                  p="8px 15px 10px"
                  variant="MenuLabel"
                  onClick={onLogout}
                  sx={{
                    opacity: 0.8,
                  }}
                >
                  Logout
                </Text>
              </Flex>
            </Flex>
          ) : (
            <Link href="/login" passHref>
              <Flex
                px={13}
                sx={{
                  cursor: "pointer",
                  height: "100%",
                  alignItems: "center",
                  justifyContent: "center",
                  ":hover": {
                    svg: { fill: "productType1", transition: "0.3s" },
                  },
                }}
              >
                <LoginIcon height={30} width={30} fill="productType2" />
              </Flex>
            </Link>
          )}
        </Flex>
      </Flex>
    </Flex>
  );
};

export default memo(Navigation);
