import { Flex, Radio, Text, Label, BoxProps, Box } from "@theme-ui/components";
import Popover from "react-popover";
import { useRouter } from "next/router";
import React, {
  FC,
  memo,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import useOnClickOutside from "../hooks/useOnClickOutside";
import { ParsedUrlQuery } from "querystring";
import { ArrowMenuIcon } from "../public/icon";

export interface SelectItemProps {
  id: string;
  label: string;
  value: string[];
  queryName: string[];
}
interface SelectProps extends BoxProps {
  selectItem: SelectItemProps[];
  name: string;
}

const FilterItem: FC<SelectProps> = ({ selectItem, name }) => {
  const ref = useRef<HTMLDivElement | null>(null);
  useOnClickOutside(ref, () => setIsOpen(false));
  const [value, setValue] = useState<string>();
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const router = useRouter();
  const { query } = router;

  useEffect(() => {
    if (router.asPath === "/products") setValue(name);
  }, [router.asPath]);

  const onOpen = useCallback(() => {
    setIsOpen((prev) => !prev);
  }, []);

  const onQuery = useCallback(
    (query?: ParsedUrlQuery) =>
      router.push(
        {
          pathname: `/products`,
          query,
        },
        undefined,
        { shallow: true }
      ),
    [router]
  );

  const onFilter = useCallback(
    (item: SelectItemProps, name: string) => {
      setValue(item.label === "All" ? name : item.label);
      item.value[0] === "all"
        ? item.queryName.map((x) => delete query[x])
        : item.queryName.map((x, index) =>
            item.value[index] === ""
              ? delete query[x]
              : (query[x] = item.value[index])
          );
      onQuery(query);
    },
    [query, onQuery]
  );
  return (
    <Popover
      onOuterAction={() => setIsOpen(false)}
      isOpen={isOpen}
      place="below"
      tipSize={0.01}
      style={{ zIndex: 3 }}
      body={
        <Flex
          ml={5}
          bg="white"
          p="15px 30px 5px 20px"
          sx={{
            flexDirection: "column",
            borderRadius: 3,
            boxShadow: "rgb(137 140 149 / 50%) 1px 4px 5px",
            transition: "0.3s",
            zIndex: 10,
          }}
        >
          {selectItem.map((item) => (
            <Label
              key={item.id}
              sx={{
                alignItems: "center",
                whiteSpace: "nowrap",
                letterSpacing: "0.5px",
                fontSize: 14,
                color: "filter",
                transition: "0.3s",
                cursor: "pointer",
                opacity: 0.8,
              }}
            >
              <Radio
                checked={item.queryName.every((x, index) =>
                  query[x]
                    ? query[x] === item.value[index]
                    : item.value[0] === "all" || item.value[index] === ""
                )}
                name={name}
                value={item.label}
                onChange={() => onFilter(item, name)}
                sx={{
                  color: "productType1",
                  mr: 5,
                }}
              />
              {item.label}
            </Label>
          ))}
        </Flex>
      }
    >
      <Text
        variant="filter"
        onClick={onOpen}
        sx={{
          boxShadow: isOpen && "rgb(137 140 149 / 50%) 0 0 15px",
        }}
      >
        {value ? value : name}{" "}
        <Box pl={15}>
          <ArrowMenuIcon width={10} height={10} />
        </Box>
      </Text>
    </Popover>
  );
};

export default memo(FilterItem);
