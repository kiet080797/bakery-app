import { Button, Flex, Grid, Text } from "@theme-ui/components";
import Link from "next/link";
import React, { FC } from "react";
import { CloseIcon } from "../public/icon";

interface PopupProps {
  onShow?: () => void;
  type: string;
  labelButton: string;
  href: string;
  content: string;
}

const Popup: FC<PopupProps> = ({
  onShow,
  type,
  labelButton,
  href,
  content,
}) => {
  return (
    <Flex
      sx={{
        mx: 15,
        bg: "white",
        width: 600,
        minHeight: 150,
        borderRadius: 30,
        overflow: "hidden",
        flexDirection: "column",
      }}
    >
      <Flex
        p="10px 30px"
        bg="productType7"
        sx={{
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
        }}
      >
        <Text
          sx={{
            fontSize: 18,
            fontWeight: 600,
            color: "productType1",
            letterSpacing: "0.5px",
          }}
        >
          {type}
        </Text>
        <Flex
          p={5}
          onClick={onShow}
          sx={{
            cursor: "pointer",
            transition: "0.3s",
            ":hover": {
              color: "productType1",
            },
          }}
        >
          <CloseIcon width={13} height={13} />
        </Flex>
      </Flex>
      <Text
        m="20px 30px"
        sx={{
          fontSize: 16,
          color: "productPriceOld",
          letterSpacing: "0.5px",
        }}
      >
        {content}
      </Text>
      <Grid columns={2} gap={10} p="10px 30px 30px">
        <Button
          onClick={onShow}
          p="12px 0"
          variant="primary"
          sx={{
            color: "productPriceOld",
            borderRadius: 20,
          }}
        >
          Back
        </Button>
        <Link href={href} passHref>
          <Button
            variant="secondary"
            sx={{
              borderRadius: 20,
            }}
            onClick={onShow}
          >
            {labelButton}
          </Button>
        </Link>
      </Grid>
    </Flex>
  );
};

export default Popup;
