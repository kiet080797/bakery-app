import { Box, BoxProps, Flex, Text } from "@theme-ui/components";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { FC, memo, useCallback, useRef, useState } from "react";
import useOnClickOutside from "../hooks/useOnClickOutside";
import { elements, menuItems } from "../pages";
import ElementsCard from "./ElementsCard";
import ItemMenu from "./ItemMenu";
import logo from "../public/images/logo.png";
import { ArrowMenuIcon, CloseIcon, MenuIcon } from "../public/icon";
export interface ItemProps {
  label: string;
  id?: string;
  content?: string[];
}

const Menu: FC<BoxProps> = ({ ...BoxProps }) => {
  const [isOpen, setIsOpen] = useState<Boolean>(false);
  const router = useRouter();
  const ref = useRef<HTMLDivElement | null>(null);
  useOnClickOutside(ref, () => setIsOpen(false));
  const handleMenu = useCallback(() => {
    setIsOpen(!isOpen);
  }, [isOpen]);

  return (
    <Flex {...BoxProps}>
      {/* Mobile Menu */}
      <Flex ref={ref}>
        <Flex
          onClick={handleMenu}
          sx={{
            px: [30, 30, 30, 30, 0],
            position: "relative",
            alignItems: "center",
            justifyContent: "center",
            opacity: [1, 1, 1, 1, 0],
            transform: [
              "scale(1)",
              "scale(1)",
              "scale(1)",
              "scale(1)",
              "scale(0)",
            ],
            transition: "0.5s",
            cursor: "pointer",
            ":hover": {
              svg: { fill: "productType1" },
            },
          }}
        >
          <Flex
            sx={{
              position: "absolute",
              left: 15,
              opacity: isOpen ? 0 : 1,
              transform: isOpen ? "rotate(-0.3turn)" : "rotate(0)",
              transition: "0.3s",
            }}
          >
            <MenuIcon width={25} height={25} />
          </Flex>
          <Flex
            sx={{
              position: "absolute",
              left: 15,
              opacity: isOpen ? 1 : 0,
              transform: isOpen ? "rotate(0)" : "rotate(0.3turn)",
              transition: "0.3s",
            }}
          >
            <CloseIcon width={20} height={20} />
          </Flex>
        </Flex>
        <Flex
          sx={{
            bg: "white",
            position: "fixed",
            top: [72, 72, 72, 72, 192],
            bottom: 0,
            width: 300,
            transform: ["none", "none", "none", "none", "translateX(-350px)"],
            flexDirection: "column",
            transition: "0.3s",
            borderRadius: "4px 0 0 4px",
            left: isOpen ? 0 : -350,
            boxShadow: "#959da5 0px 12px 24px 0px",
            zIndex: 50,
          }}
        >
          <Flex
            sx={{
              flexDirection: "column",
              overflowY: "auto",
              "::-webkit-scrollbar": {
                width: 7,
              },
              "::-webkit-scrollbar-thumb": {
                bg: "productType1",
                borderRadius: 5,
              },
            }}
          >
            {menuItems.map((item) => (
              <Flex key={item.id}>
                {item.id !== "6" ? (
                  <ItemMenu
                    label={item.label}
                    content={item.content}
                    id={item.id}
                  />
                ) : (
                  <Flex
                    sx={{
                      width: "100%",
                      flexDirection: "column",
                    }}
                  >
                    <ItemMenu
                      label="ELEMENT"
                      content={elements.element}
                      id={item.id}
                    />
                    <ItemMenu
                      id={item.id}
                      label="ADDITIONAL PAGE"
                      content={elements.additional}
                    />
                    <ItemMenu label="WELCOME" id={item.id} />
                  </Flex>
                )}
              </Flex>
            ))}
          </Flex>
          <Link href="/" passHref>
            <Box
              sx={{
                py: 4,
                mt: "auto",
                bg: "white",
                height: 70,
                objectFit: "scale-down",
                width: "100%",
                borderTop: "1px solid #e6e6e6",
                cursor: "pointer",
                textAlign: "center",
                zIndex: 50,
              }}
            >
              <Image
                src={logo}
                placeholder="blur"
                alt="images/logo.png"
                height={60}
                width={250}
              />
            </Box>
          </Link>
        </Flex>
      </Flex>
      {/* Window Menu  */}
      <Flex
        sx={{
          height: 70,
          transition: "0.3s",
          opacity: [0, 0, 0, 0, 1],
          transform: [
            "scale(0)",
            "scale(0)",
            "scale(0)",
            "scale(0)",
            "scale(1)",
          ],
        }}
      >
        {menuItems.map((item) => (
          <Flex
            mx={20}
            key={item.id}
            sx={{
              transition: "0.3s",
              position: "relative",
              alignItems: "center",
              ":before": {
                content: "''",
                position: "absolute",
                left: -20,
                top: 0,
                height: "8px",
                width: item.content ? "calc(100% + 20px)" : "calc(100% + 40px)",
                bg:
                  router.pathname === "/" && item.id === "1"
                    ? "productType1"
                    : "transparent",
                transition: "0.6s",
              },
              ":hover": {
                color: "productType1",
                ":before": {
                  left: 0,
                  width: item.content ? "calc(100% - 20px)" : "100%",
                  bg: "productType1",
                },
                ".popover": {
                  opacity: 1,
                  transform: "scale(1)",
                },
              },
            }}
          >
            <Text variant="popupLabel">{item.label}</Text>
            {item.content && (
              <Flex ml={5}>
                <ArrowMenuIcon width={10} height={10} />
              </Flex>
            )}
            {item.content && (
              <Flex
                className="popover"
                bg="white"
                sx={{
                  position: "absolute",
                  opacity: 0,
                  top: "100%",
                  left: item.id === "6" ? "-540%" : -40,
                  transform: "scale(0)",
                  transition: "0.4s",
                  boxShadow: "0 10px 25px 5px rgb(1 3 4 / 20%)",
                  zIndex: 2,
                }}
              >
                {item.id === "6" ? (
                  <ElementsCard elements={elements} />
                ) : (
                  <Flex
                    p="40px 40px 20px"
                    sx={{
                      width: 250,
                      flexDirection: "column",
                    }}
                  >
                    {item.content.map((x, index) => (
                      <Text key={index} variant="popupContent">
                        <Link href="#" passHref>
                          {x}
                        </Link>
                      </Text>
                    ))}
                  </Flex>
                )}
              </Flex>
            )}
          </Flex>
        ))}
      </Flex>
    </Flex>
  );
};

export default memo(Menu);
