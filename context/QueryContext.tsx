import { useRouter } from "next/router";
import React, {
  createContext,
  Dispatch,
  SetStateAction,
  useEffect,
  useState,
} from "react";
import { params } from "../queries/fetchData";

export interface QueryContextProps {
  query: params;
  setQuery: Dispatch<SetStateAction<params>>;
}

const QueryContext = createContext<QueryContextProps>({
  setQuery: null,
  query: null,
});

export const QueryProvider = (props) => {
  const router = useRouter();
  const [query, setQuery] = useState<params>({});

  useEffect(() => {
    setQuery(router.query);
  }, [router.query]);

  return (
    <QueryContext.Provider value={{ query, setQuery }}>
      {props.children}
    </QueryContext.Provider>
  );
};

export default QueryContext;
