import { useMutation, useQuery } from "react-query";
import { axiosClient } from ".";
import { LoginPayload, Profile, RegisterPayload } from "../models";

//login
export function useLoginMutation() {
  async function login(payload: LoginPayload) {
    return await axiosClient.post("/login", {
      identifier: payload.email,
      password: payload.password,
    });
  }
  return useMutation(login);
}

//logout
export function logout() {
  return axiosClient.post("/logout");
}

//register
export function useRegisterMutation() {
  async function register(payload: RegisterPayload) {
    return await axiosClient.post("/register", {
      lastName: payload.lastName,
      firstName: payload.firstName,
      username: payload.username,
      email: payload.email,
      password: payload.password,
    });
  }
  return useMutation(register);
}

//get profile
export function useProfileQuery(isLogin: boolean) {
  async function getProfile(): Promise<Profile> {
    return await axiosClient.post("/profiles");
  }

  return useQuery("profile", () => getProfile(), {
    enabled: isLogin,
  });
}
