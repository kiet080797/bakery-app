import { GetServerSideProps, GetServerSidePropsContext } from "next";
import { dehydrate, QueryClient } from "react-query";

interface Context extends GetServerSidePropsContext {
  queryClient: QueryClient;
}

export const getServerSidePropsFunc = <P>(
  fetchProps: (context: Context) => Promise<P>
): GetServerSideProps<P> => {
  return async (context: GetServerSidePropsContext) => {
    const queryClient = new QueryClient();
    const props = await fetchProps({ ...context, queryClient });
    return {
      props: {
        dehydratedState: dehydrate(queryClient),
        ...props,
      },
    };
  };
};
